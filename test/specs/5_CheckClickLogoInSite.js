/*
 * Copyright (c) 1993-2022. All rights reserved.
 *
 * This software is confidential and proprietary information.
 * ("Confidential information"). You must not disclose such Confidential Information
 * and use it only in accordance with the terms of the license agreement.
 */
const fetch = require('node-fetch');
const links = [];
const assert = require('assert')
const url = 'https://wldnwaddev.wpengine.com';
const {addStep} = require('@wdio/allure-reporter').default
const {addEnvironment} = require('@wdio/allure-reporter').default

/**
 * Test for the site .
 *
 * @version   1.01 14_07_2022
 * @author    Potip Serhii
 */
describe('5.Check click logo in Site', async () => {

    it('1 Check click logo in Site', async () => {

        const result = await fetch(`${url}/wp-json/wp/v2/pages/?per_page=100`);
        const pages = await result.json();
        pages.forEach(page => links.push(page.link));

        for (const responsesAll of links) {
            await browser.url(responsesAll);
            console.log(">>>>>>>>>>>>>>" + responsesAll)

            if (responsesAll !== (url + '/')) {
                const classNameAndPartialText = await $('.logo').$('a')
                console.log(await classNameAndPartialText.getAttribute('href'))
                await expect(classNameAndPartialText).toHaveHref(url)
            }
        }
    })
});