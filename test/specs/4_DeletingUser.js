/*
 * Copyright (c) 1993-2022. All rights reserved.
 *
 * This software is confidential and proprietary information.
 * ("Confidential information"). You must not disclose such Confidential Information
 * and use it only in accordance with the terms of the license agreement.
 */
const assert = require('assert')
const url = '/';
const fetch = require('node-fetch');
const {addStep} = require('@wdio/allure-reporter').default
const {addEnvironment} = require('@wdio/allure-reporter').default

/**
 * Test for the site .
 *
 * @version   1.01 14_07_2022
 * @author    Potip Serhii
 */
describe('4.Deleting a registered user', async () => {

    before(async () => {
        addEnvironment('browserName: ', 'Chrome');
        addEnvironment('browserVersion: ', '105.0.5195.127')
        addEnvironment('OS: ', 'Win 10 x64')
    });

    /** Test 1 */
    it('1.Deleting a registered user', async () => {

        //   await browser.maximizeWindow();
        await browser.setWindowSize(1920, 1080);

        //
        // const idLogOut = await $("a[href='/?logout=true']")
        // addStep('8. Нашли кнопку logout');
        //
        // await idLogOut.click()
        // addStep('9. Нажали на кнопку logout');


        const url = 'https://wldnwaddev.wpengine.com/account/login/';
        await browser.url(url);
        addStep('1. Перешли на главную страницу login');

        const myButton = await $("#cookie_action_close_header")
        addStep('2. Нашли cookie_action_close_header');

        await myButton.click();
        addStep('4. Нажалли на кнопку Accept');

        const idNameEmailLogIn = await $("#email_address")
        addStep('2. Нашли поле Email Address');

        await idNameEmailLogIn.addValue('test888@gmail.com')
        addStep('3. Ввели в поле Email Address test888@gmail.com');

        const idNamePassword = await $('#password')
        addStep('4. Нашли поле password');

        await idNamePassword.addValue('123456')
        addStep('5. Ввели в поле password 123456');

        await browser.pause(4000)

        const idButtonNextPage1 = await $(".sign-in-button")
        addStep('6. Нашли кнопку Join Now');

        await idButtonNextPage1.click()
        addStep('7. Нажали на кнопку sign-in-button');

        const idProfileButton = await $(".profile_button")
        addStep('8. Нашли кнопку profile_button');

        await idProfileButton.click()
        addStep('9. Нажали на кнопку profile_button');

        const idAccountSettings = await $("a[href='/account/edit-profile']")
        addStep('10. Нашли кнопку Account Settings');

        await idAccountSettings.click()
        addStep('11. Нажали на кнопку Account Settings');

        const idButtonDelAcc = await $(".btn-delete-account")
        addStep('12. Нашли ссылку delete-account');

        await idButtonDelAcc.click()
        addStep('13. Нажали на кнопку delete-account');

        const classRadioButton = await $("#delete-acc-8")
        addStep('14. Нашли радиобаттон');

        await classRadioButton.click()
        addStep('15. Нажали на радиобаттон');

        const idButtonDelete = await $(".btn.delete-my-account")
        addStep('16. Нашли ссылку delete-account');

        await idButtonDelete.click()
        addStep('17. Нажали на кнопку delete-account');

        await browser.pause(8000)

        const idGotIt = await $("div[class='content-wrapper'] button[type='button']")
        addStep('18. Нашли кнопку Got It');

        await idGotIt.click()
        addStep('19. Нажали на кнопку Got It');

        await browser.pause(4000)

    })
});