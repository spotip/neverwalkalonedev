/*
 * Copyright (c) 1993-2022. All rights reserved.
 *
 * This software is confidential and proprietary information.
 * ("Confidential information"). You must not disclose such Confidential Information
 * and use it only in accordance with the terms of the license agreement.
 */
const assert = require('assert')
const url = '/';
const fetch = require('node-fetch');
const {addStep} = require('@wdio/allure-reporter').default
const {addEnvironment} = require('@wdio/allure-reporter').default

/**
 * Test for the site .
 *
 * @version   1.01 14_07_2022
 * @author    Potip Serhii
 */
describe('2.Upgrade Your Account', async () => {

    before(async () => {
        addEnvironment('browserName: ', 'Chrome');
        addEnvironment('browserVersion: ', '105.0.5195.127')
        addEnvironment('OS: ', 'Win 10 x64')
    });

    /** Test 1 */
    it('1.Upgrade Your Account', async () => {

        await browser.setWindowSize(1920, 1080);
        const url = 'https://wldnwaddev.wpengine.com/account/login/';
        await browser.url(url);
        addStep('1. Перешли на главную страницу login');

        const myButton = await $("#cookie_action_close_header")
        addStep('2. Нашли cookie_action_close_header');

        await myButton.click();
        addStep('4. Нажалли на кнопку Accept');

        const idNameEmailLogIn = await $("#email_address")
        addStep('2. Нашли поле Email Address');

        await idNameEmailLogIn.addValue('test888@gmail.com')
        addStep('3. Ввели в поле Email Address test888@gmail.com');

        const idNamePassword = await $('#password')
        addStep('4. Нашли поле password');

        await idNamePassword.addValue('123456')
        addStep('5. Ввели в поле password 123456');

        await browser.pause(4000)

        const idButtonNextPage1 = await $(".sign-in-button")
        addStep('6. Нашли кнопку Join Now');

        await idButtonNextPage1.click()
        addStep('7. Нажали на кнопку sign-in-button');


        const idProfileButton = await $(".profile_button")
        addStep('8. Нашли кнопку profile_button');

        await idProfileButton.click()
        addStep('9. Нажали на кнопку profile_button');



        const idUpgradeAccountButton = await $("li[class='item-upgrade-account'] a")
        addStep('10. Нашли кнопку UpgradeAccount');

        await idUpgradeAccountButton.click()
        addStep('11. Нажали на кнопку UpgradeAccount');

        const idSelectButton = await $("button[data-amount='19.99']")
        addStep('12. Нашли кнопку Select');

        await idSelectButton.click()
        addStep('13. Нажали на кнопку Select');

        const idCartNumber = await  $("iframe[title='Защищенное окно для ввода данных оплаты картой']")
        addStep('14. Нашли поле CartNumber');
        await browser.pause(4000)
        await idCartNumber.addValue('4242424242424242122412312345')
        addStep('15. Ввели в поле CartNumber 4242424242424242');


        const idPaymentButton = await $(".btn-checkout-payment")
        addStep('16. Нашли кнопку Payment');

        await idPaymentButton.click()
        addStep('17. Нажали на кнопку Payment');

        await browser.pause(4000)

        const idDashboardButton = await $('=Dashboard')
        addStep('18. Нашли кнопку Dashboard');

        await idDashboardButton.click()
        addStep('19. Нажали на кнопку Dashboard');

        const classNameAndText = await $("span[data-value='premium']")
        addStep('20. Checking Premium');

        await expect(classNameAndText).toHaveText('Premium')
        addStep('21. Checking text Premium');

        await browser.pause(4000)
    })
})