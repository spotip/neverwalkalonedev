/*
 * Copyright (c) 1993-2022. All rights reserved.
 *
 * This software is confidential and proprietary information.
 * ("Confidential information"). You must not disclose such Confidential Information
 * and use it only in accordance with the terms of the license agreement.
 */
const assert = require('assert')
const url = '/';

const fetch = require('node-fetch');
const links = [];
const {addStep} = require('@wdio/allure-reporter').default
const {addEnvironment} = require('@wdio/allure-reporter').default

/**
 * Test for the site .
 *
 * @version   1.01 14_07_2022
 * @author    Potip Serhii
 */
describe('6.The presence in the site URL of the word "Page" or "page"', async () => {

    before(async () => {
        addEnvironment('browserName: ', 'Chrome');
        addEnvironment('browserVersion: ', '103.0.5060.134')
        addEnvironment('OS: ', 'Win 10 x64')
    });

    /** Test 1.1 */
    it('1.1 The presence in the site URL of the word "Page" or "page"', async () => {

        await browser.url(url);
        const getURL = await browser.getUrl();
        const result = await fetch(`${getURL}wp-json/wp/v2/pages/?per_page=100`);

        const pages = await result.json();
        await pages.forEach(page => links.push(page.link));

        const errorsUrls = [];
        for (const responsesAll of links) {
            //console.log(responsesAll)
            if (responsesAll.includes('page') || responsesAll.includes('Page')) {
                errorsUrls.push(responsesAll);
            }
        }
        if (errorsUrls.length > 0) {
            throw new Error(`URLs page >>>>>>>>>>  ${errorsUrls.join('\r\n')}`);
        }
    })
});