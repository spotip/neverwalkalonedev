/*
 * Copyright (c) 1993-2022. All rights reserved.
 *
 * This software is confidential and proprietary information.
 * ("Confidential information"). You must not disclose such Confidential Information
 * and use it only in accordance with the terms of the license agreement.
 */
const assert = require('assert')
const url = '/';
const fetch = require('node-fetch');
const {addStep} = require('@wdio/allure-reporter').default
const {addEnvironment} = require('@wdio/allure-reporter').default

/**
 * Test for the site .
 *
 * @version   1.01 14_07_2022
 * @author    Potip Serhii
 */
describe('3.Deleting Credit Card', async () => {

    before(async () => {
        addEnvironment('browserName: ', 'Chrome');
        addEnvironment('browserVersion: ', '105.0.5195.127')
        addEnvironment('OS: ', 'Win 10 x64')
    });

    /** Test 1 */
    it('1.Deleting Credit Card', async () => {

        await browser.setWindowSize(1920, 1080);
        const url = 'https://wldnwaddev.wpengine.com/account/login/';
        await browser.url(url);
        addStep('1. Перешли на главную страницу login');

        const myButton = await $("#cookie_action_close_header")
        addStep('2. Нашли cookie_action_close_header');

        await myButton.click();
        addStep('3. Нажалли на кнопку Accept');

        const idNameEmailLogIn = await $("#email_address")
        addStep('4. Нашли поле Email Address');

        await idNameEmailLogIn.addValue('test888@gmail.com')
        addStep('5. Ввели в поле Email Address test888@gmail.com');

        const idNamePassword = await $('#password')
        addStep('6. Нашли поле password');

        await idNamePassword.addValue('123456')
        addStep('7. Ввели в поле password 123456');

        await browser.pause(4000)

        const idButtonNextPage1 = await $(".sign-in-button")
        addStep('8. Нашли кнопку Join Now');

        await idButtonNextPage1.click()
        addStep('9. Нажали на кнопку sign-in-button');


        const idProfileButton = await $(".profile_button")
        addStep('10. Нашли кнопку profile_button');

        await idProfileButton.click()
        addStep('11. Нажали на кнопку profile_button');

        const idEditProfileButton = await $("a[href='/account/edit-profile']")
        addStep('14. Нашли кнопку Account Settings');

        await idEditProfileButton.click()
        addStep('15. Нажали на кнопку Account Settings');

        const idManageSubscriptionButton = await $("button[data-value='manage-subscription']")
        addStep('16. Нашли кнопку Manage Subscription');

        await idManageSubscriptionButton.click()
        addStep('17. Нажали на кнопку Manage Subscription');

        const idDeleteCardButton = await $(".delete-card-btn")
        addStep('18. Нашли кнопку Manage DeleteCard');

        await idDeleteCardButton.click()
        addStep('19. Нажали на кнопку DeleteCard');

        const idYesDeleteCardButton = await $("div[id='edit-profile-delete'] div[class='buttons-wrap'] button:nth-child(1)")
        addStep('18. Нашли кнопку Yes, delete it');

        await idYesDeleteCardButton.click()
        addStep('19. Нажали на кнопку Yes, delete it');

        await browser.pause(4000)
    })
})