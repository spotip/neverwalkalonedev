/*
 * Copyright (c) 1993-2022. All rights reserved.
 *
 * This software is confidential and proprietary information.
 * ("Confidential information"). You must not disclose such Confidential Information
 * and use it only in accordance with the terms of the license agreement.
 */
const assert = require('assert')
const url = '/';
const fetch = require('node-fetch');
const {addStep} = require('@wdio/allure-reporter').default
const {addEnvironment} = require('@wdio/allure-reporter').default

/**
 * Test for the site .
 *
 * @version   1.01 14_07_2022
 * @author    Potip Serhii
 */
describe('1.User registration', async () => {

    before(async () => {
        addEnvironment('browserName: ', 'Chrome');
        addEnvironment('browserVersion: ', '105.0.5195.127')
        addEnvironment('OS: ', 'Win 10 x64')
    });

    /** Test 1 */
    it('1 Checking button "Accept-cookie" ', async () => {
        await browser.url(url);
        addStep('1. Перешли на главную страницу');
//        await browser.deleteAllCookies();
        const myButton = await $("#cookie_action_close_header")
        addStep('2. Нашли cookie_action_close_header');

        await expect(myButton).toHaveText('Accept')
        addStep('3. Нашли название кнопки Accept');

        await myButton.click();
        addStep('4. Нажалли на кнопку Accept');
    });

    /** Test 2 */
    it('2.Registration of a user from the main page', async () => {
        //   await browser.maximizeWindow();
        await browser.setWindowSize(1920, 1080);
        await browser.url(url);
//        await browser.getWindowSize()
        addStep('1. Перешли на главную страницу');



        const classNameJoinNow = await $('.join-now-button')
        addStep('4. Нашли кнопку Join Now');

        await classNameJoinNow.click()
        addStep('5. Нажали на кнопку Join Now');


        const classNameActive = await $("button[data-value='man']")
        addStep('2. Нашли кнопку Man');

        await classNameActive.click()
        addStep('3. Нажали на кнопку Man');


        const idNameEmail = await $('#email-address')
        addStep('6. Нашли поле Email Address');

        await idNameEmail.addValue('test888@gmail.com')
        addStep('7. Ввели в поле Email Address test888@gmail.com');

        const idNamePassword = await $('#password')
        addStep('8. Нашли поле password');

        await idNamePassword.addValue('123456')
        addStep('9. Ввели в поле password 123456');

        const idConfirmPassword = await $('#confirm-password')
        addStep('10. Нашли поле confirm-password');

        await idConfirmPassword.addValue('123456')
        addStep('11. Ввели в поле confirm-password 123456');

        const idAgreeTermsConditionsAgree = await $("label[for=\'agree_terms_conditions_agree\']")
        addStep('12. Нашли поле agree_terms_conditions_agree');

        await idAgreeTermsConditionsAgree.click({x: -10})
        addStep('13. Кликнули по чекбоксу agree_terms_conditions_agree');

        const idButtonNextPage1 = await $('#button-next-page-1')
        addStep('14. Нашли кнопку Join Now');

        await idButtonNextPage1.click()
        addStep('15. Нажали на кнопку Join Now');
        await browser.pause(5000)

        const idFirstName = await $('#first-name')
        addStep('16. Нашли поле first-name');

        await idFirstName.addValue('test')
        addStep('17. Ввели в поле first-name test');

        const idLastName = await $('#last-name')
        addStep('18. Нашли поле last-name');

        await idLastName.addValue('test')
        addStep('19. Ввели в поле last-name test');


        const idUserName = await $("#username")
        addStep('18. Нашли поле UserName');

        await idUserName.addValue('test888')
        addStep('19. Ввели в поле UserName test888');



        const idBirthdayMonth = await $('#birthday-month')
        addStep('20. Нашли поле birthday-month');

        await idBirthdayMonth.addValue('8')
        addStep('21. Ввели в поле birthday-month 8');

        const idBirthdayDay = await $('#birthday-day')
        addStep('22. Нашли поле birthday-day');

        await idBirthdayDay.addValue('8')
        addStep('23. Ввели в поле birthday-day 8');

        const idBirthdayYear = await $('#birthday-year')
        addStep('24. Нашли поле birthday-year');

        await idBirthdayYear.addValue('2000')
        addStep('25. Ввели в поле birthday-year 2000');

        // const idZipCode = await $('#zip-code')
        // addStep('24. Нашли поле zip-code');
        //
        // await idZipCode.addValue('57001')
        // addStep('25. Ввели в поле zip-code 57001');

        const idButtonNextPage2 = await $('#button-next-page-2')
        addStep('26. Нашли кнопку Next');

        await idButtonNextPage2.click()
        addStep('27. Нажали на кнопку Next');
        await browser.pause(4000)


        const select2MaritalStatusContainer = await $("#select2-marital-status-container")
        addStep('28. Нашли поле select2-marital-status-container');

        await select2MaritalStatusContainer.click()
        await browser.pause(2000)
        await select2MaritalStatusContainer.click({x: 30, y: 40})
        addStep('29. Кликнули по чекбоксу marital-status');

        const idBornAgain = await $("#select-born-yes")
            //$('#block-select-born')
        addStep('30. Нашли поле block-select-born');

        await idBornAgain.click()
        addStep('31. Кликнули по чекбоксу born-again');

        const select2ChurchAttendanceContainer = await $("option[value='Attend church every week']")
        addStep('32. Нашли поле select2-church-attendance-container');

        await select2ChurchAttendanceContainer.click()
        // await browser.pause(2000)
        // await select2ChurchAttendanceContainer.click({x: 30, y: 40})
        addStep('33. Кликнули по всплывающему меню church-attendance');

        const idHaveKids = await $('#block-select-kids')
        addStep('34. Нашли поле block-select-kids');

        await idHaveKids.click()
        addStep('35. Кликнули по чекбоксу hove-kids');

        const select2LevelOfEducationContainer = await $("option[value='definitely']")
        addStep('36. Нашли поле select2-level-of-education-container');

        await select2LevelOfEducationContainer.click()
        addStep('37. Кликнули по всплывающему меню level-of-education');
        // await select2LevelOfEducationContainer.click({x: 30, y: 40})
        // addStep('39. Кликнули по всплывающему меню level-of-education');

        const idOccupation = await $('#occupation')
        addStep('38. Нашли поле occupation');

        await idOccupation.addValue('test')
        addStep('39. Ввели в поле occupation test');

        const select2WantKidsContainer = await $("option[value='high_school']")
        addStep('40. Нашли поле select2-want-kids-container');

        await select2WantKidsContainer.click()
        await browser.pause(2000)
        // await select2WantKidsContainer.click({x: 30, y: 40})
        addStep('41. Кликнули по всплывающему меню want-kids');


        const idButtonNextPage3 = await $('#button-next-page-3')
        addStep('42. Нашли кнопку Next');

        await browser.pause(4000)

        await idButtonNextPage3.click()
        addStep('43. Нажали на кнопку Next');

        const idButtonNextPage4 = await $('#button-next-page-4')
        addStep('44. Нашли кнопку Next');

        await idButtonNextPage4.click()
        addStep('45. Нажали на кнопку Next');
        await browser.pause(4000)

        const classBornAgainLooking = await $("#born-again-looking-yes")
        addStep('46. Нашли поле block-select-born');

        await classBornAgainLooking.click()
        addStep('47. Кликнули по чекбоксу block-select-born');

        const classBornAgainLooking2 = await $("#want-born-again-yes")
        addStep('48. Нашли поле block-select-born');

        await classBornAgainLooking2.click()
        addStep('49. Кликнули по чекбоксу block-select-born');

        const idSearchGlobally = await $('#search_globally')
        addStep('50. Нашли радиобокс search_globally');

        await idSearchGlobally.click()
        addStep('51. Нажали на радиобокс search_globally');

        const idButtonNextPage5 = await $('#button-next-page-5')
        addStep('52. Нашли кнопку Next');

        await idButtonNextPage5.click()
        addStep('53. Нажали на кнопку Next');

        await browser.pause(4000)

        // const idButtonNextPage6 = await $('#button-next-page-6')
        // addStep('54. Нашли кнопку Next');
        //
        // await idButtonNextPage6.click()
        // addStep('55. Нажали на кнопку Next');

        const idButtonNextPage7 = await $("a[href='https://wldnwaddev.wpengine.com/account/register/']")
        addStep('56. Нашли кнопку Sign in');

        await idButtonNextPage7.click()
        addStep('57. Нажали на кнопку Sign in');

        const idProfileButton = await $(".profile_button")
        addStep('58. Нашли кнопку profile_button');

        await idProfileButton.click()
        addStep('59. Нажали на кнопку profile_button');

        const idLogOut = await $("li[class='item-sign-out'] a")//("a[href='/?logout=true']")

        addStep('60. Нашли кнопку logout');

        await idLogOut.click()
        addStep('61. Нажали на кнопку logout');
    });
})